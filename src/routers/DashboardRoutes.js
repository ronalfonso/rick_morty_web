import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";

import Navbar from "../UI/Navbar";
import {List} from "../containers/List";


export const DashboardRoutes = () => {

    return (
        <>
            <Navbar/>
            <div>
                <Switch>
                    <Route exact path="/list" component={List}/>

                    <Redirect to="/list"/>
                </Switch>
            </div>

        </>
    )

}
