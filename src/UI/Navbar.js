import React, {useContext} from 'react';
import {useHistory} from 'react-router-dom'
import {AuthContext} from "../auth/AuthContext";
import Button from 'react-bootstrap/Button';
import {types} from "../types/types";

export const Navbar = () => {

    const {user: {name, lastName}, dispatch} = useContext(AuthContext);
    const history = useHistory();

    const handleLogout = () => {
        dispatch({
            type: types.logout
        });
        history.replace('/login')

    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between navbar-fixed">
            <a className="navbar-brand" href="#">Rick & Morty</a>
            <div className="flex-display">
                <span className="nav-link nav-item text-capitalize navbar-brand">
                    {name} {lastName}
                </span>

                <Button className="nav-item nav-link btn"
                    onClick={handleLogout}
                >
                    Logout
                </Button>
            </div>
        </nav>


    )

}

export default Navbar;
