import React from 'react';
import BaseService from "./BaseService";
import axios from 'axios';

export default class UserService extends React.Component {
    static getClassUrl() {
        return 'users'
    }

    static getUrl() {
        return `${BaseService.getBaseUrl()}${UserService.getClassUrl()}`;
    }

    static createUser(body){
        return axios.post(`${UserService.getUrl()}`, body).then(res => {
            return res.data
        }, (error) => {
            console.error({error});
            return error
        }).catch(err => {
            if (err.response) {
                return err
            }

        })

    }

    static login(body) {
        let headers = {
            username: body.username,
            password: body.password
        }
        return axios.get(`${UserService.getUrl()}`,  {headers}).then(res => {
            return res.data
        }, (error) => {
            console.error({error});
            return error
        }).catch(err => {
            if (err.response) {
                return err
            }
        })
    }
}
