import React, {useContext, useState} from 'react';
import './styles.css';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {useForm} from "../hooks/useForm";
import UserService from "../api/UserService";
import {AuthContext} from "../auth/AuthContext";
import {types} from "../types/types";


const Login = ({history}) => {

    const {dispatch} = useContext(AuthContext);

    const [formValues, handleInputChange, reset] = useForm({
        name: '',
        lastName: '',
        username: '',
        password: '',
        usernameRegister: '',
        passwordRegister: ''
    });

    const {
        name,
        lastName,
        username,
        password,
        usernameRegister,
        passwordRegister
    } = formValues;

    const handleCreate = (e) => {
        e.preventDefault();
        const body = {
            name: name,
            lastName: lastName,
            username: usernameRegister,
            password: passwordRegister
        }
        UserService.createUser(body).then(resp => {
            if (resp.success) {
                reset();
            }

        }, (err) => {
            console.log(err);
        })
    }

    const handleLogin = (e) => {
        e.preventDefault();
        const body = {
            username,
            password
        }
        UserService.login(body).then(resp => {
            if (resp.success) {
                reset();
                const user = resp.data;
                dispatch({
                    type: types.login,
                    payload: {
                        name: user.name,
                        lastName: user.lastName,
                        username: user.username,
                        token: user.token
                    }
                })
                history.replace('/list')
            }
        })
    }


    return (
        <>
            <div className="main-login">

            </div>
            <div className="container-card">
                <div className="card-login">
                    <div className="card-title">
                        <div>
                            <img className="img-text" src="./../../assets/img/rick-and-morty-icon.png" alt=""/>
                        </div>
                    </div>
                    <div className="card-container">
                        <div className="title">
                            <h4>¡Welcome to Rick & Morty App!</h4>
                        </div>
                    </div>
                    <div className="inputs">
                        <Tabs defaultActiveKey="login" id="uncontrolled-tab">
                            <Tab eventKey="login" title="Login">
                                <div className="container-login">
                                    <Form onSubmit={handleLogin}>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputEmail1">Username</label>
                                            <input type="text" className="form-control" id="inputUsername"
                                                   aria-describedby="emailHelp" placeholder="Enter username"
                                                   value={username} onChange={handleInputChange} name="username">

                                            </input>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="exampleInputEmail1">Password</label>
                                            <input type="password" className="form-control" id="inputLastname"
                                                   aria-describedby="emailHelp" placeholder="Enter password"
                                                   value={password} onChange={handleInputChange} name="password">

                                            </input>
                                        </div>
                                        <Button variant="primary" type="submit">
                                            Login
                                        </Button>
                                    </Form>

                                </div>
                            </Tab>
                            <Tab eventKey="registre" title="Registre">
                                <div className="container-login">
                                    <Form className="form-register" onSubmit={handleCreate}>
                                        <div className="register-left">
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Name</label>
                                                <input type="text" className="form-control" id="inputname"
                                                       aria-describedby="emailHelp" placeholder="Enter name"
                                                       value={name} onChange={handleInputChange} name="name">

                                                </input>
                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Last name</label>
                                                <input type="text" className="form-control" id="inputLastname"
                                                       aria-describedby="emailHelp" placeholder="Enter last name"
                                                       value={lastName} onChange={handleInputChange} name="lastName">

                                                </input>
                                            </div>
                                        </div>
                                        <div className="register-right">
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Username</label>
                                                <input type="text" className="form-control" id="inputUsername"
                                                       aria-describedby="emailHelp" placeholder="Enter username"
                                                       value={usernameRegister} onChange={handleInputChange} name="usernameRegister">

                                                </input>
                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Password</label>
                                                <input type="password" className="form-control" id="inputLastname"
                                                       aria-describedby="emailHelp" placeholder="Enter password"
                                                       value={passwordRegister} onChange={handleInputChange} name="passwordRegister">

                                                </input>
                                            </div>
                                        </div>
                                        <Button variant="primary" type="submit">
                                            Register
                                        </Button>
                                    </Form>

                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </div>
        </>
    )
}


export default Login;
