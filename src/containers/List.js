import React, {useContext, useEffect, useState} from 'react';
import Card from "../components/card/Card.js";
import './styles.css';
import CharacterService from '../api/CharacterService';
import {AuthContext} from "../auth/AuthContext";
import data from './../../assets/data.json';
import {Spinner} from 'react-bootstrap'

export const List = () => {


    const {user: {username, token}} = useContext(AuthContext);
    const [characters, setCharacters] = useState([]);


    useEffect(() => {
        CharacterService.getListCharacters(username, token).then(resp => {
            if (resp) {

                setCharacters(resp)


            } else {
                setCharacters(data)

            }
        })
        return () => {

        }
    }, [])

    return (
        <div className="main">

            {
                characters.length === 0 ?
                    <div className="container-spinner">
                        <Spinner animation="grow" variant="info"/>
                    </div>
                    :
                    <div className="container-list">

                        {
                            characters.map(character => {
                                return (
                                    <div className="list-cards">
                                        <Card character={character}/>
                                    </div>
                                )
                            })
                        }
                    </div>

            }

        </div>
    )

}

